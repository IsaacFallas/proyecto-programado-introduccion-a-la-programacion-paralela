#include <stdbool.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#define simTime 104

typedef struct
{
    bool sick; 							//true, si la tortuga está infectada
    unsigned int remaining_immunity;	//cuántas semanas de inmunidad le quedan a la tortuga
    unsigned int sick_time; 			//semanas que la tortuga estuvo infectada
    unsigned int age;   				//semanas de vida de la tortuga 
    unsigned int x;						//Coordenada x
    unsigned int y;						//Coordenada y
    bool immune;						//Si la tortuga es inmune

} turtle;

typedef struct
{
    turtle* tortuga;
    bool vacio; // 0 lleno, 1 vacio

} casilla; 


typedef struct
{
    float infected_percent; // % de la poblacion infectada
    float immune_percent; // % de la poblacion inmune
    unsigned int lifespan; // vida de una tortuga
    unsigned int chance_reproduce; //probabilidad de que una tortuga se reproduzca-> Entero de 0 a 100
    unsigned int carrying_capacity; //el número de tortugas que pueden estar en el mundo a la vez
    unsigned int immunity_duration; //cuantas semanas dura la inmunidad
    unsigned int count_turtles;
    unsigned int count_turtles_sicks;
    unsigned int count_turtles_inmune;
    unsigned int count_turtles_healthy;
    unsigned int duration;				//Duracion del virus. De 0 a 99 semanas
    unsigned int infectiousness;		//Infectividad (Probabilidad) Entero del 1 a 100
    unsigned int number_people;			//Numero de bichos que se crean al inicio. Numero de 10 a 300
    unsigned int chance_recover;		//Probabilidad de recuperación del virus. Entero del 1 al 100	
} globalVars;

//void setup ();

void setup_turtles(casilla** mundo, int** infectados, globalVars* globalData);

turtle* new_turtle(int** infectados, globalVars* globalData, bool first);

void get_sick(turtle* t, int** infectados, globalVars* globalData, bool first);

void get_healthy(turtle* t, int** infectados, globalVars* globalData, bool first);

void become_immune(turtle* t, globalVars* globalData, int** infectados);

void setup_constants(globalVars* globalData);

void go (casilla** mundo, int** infectados, FILE* archivo, globalVars* globalData);

void get_older (casilla* mundo, int** infectados, globalVars* globalData);

void move (turtle* t, int** infectados);

void update_global_variables(globalVars* globalData);

void infect(turtle* t, int** infectados, globalVars* globalData);

void recover_or_die (casilla* mundo, int** infectados, globalVars* globalData);

void reproduce (casilla** mundo, globalVars* globalData, int** infectados);

void die(turtle* t);

void reset_ticks();

void crear_archivo(int semana, FILE* archivo, globalVars* globalData);

//~ void tick();





