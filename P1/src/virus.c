#include "../include/virus.h"

/*void setup (casilla** mundo, int** infectados){
	setup_constants();
	setup_turtles(mundo, infectados);
	update_global_variables();
	reset_ticks();
}*/

turtle* new_turtle(int** infectados, globalVars* globalData, bool first){
	turtle* t = (turtle*)malloc(sizeof(turtle));
	t->remaining_immunity = 0;
	t->sick_time = 0;
	t->age = rand()%globalData->lifespan;
	t->x = rand()%34;
	t->y = rand()%34;
	get_healthy(t, infectados, globalData, first);
	globalData->count_turtles++;
	return t;
}

void setup_turtles(casilla** mundo, int** infectados, globalVars* globalData){
	

	time_t tiempo;
	srand((unsigned) time(&tiempo));
	
	for (int i = 0; i < globalData->number_people; i++){
		mundo[i]->vacio = 0;
	}

	for (int j = 0; j < globalData->number_people; j++){
		mundo[j]->tortuga = new_turtle(infectados, globalData, true); 
	}

	for (int k = 0; k < 10; k++){
		get_sick(mundo[k]->tortuga, infectados, globalData, false);
	}

}

void get_sick(turtle* t, int** infectados, globalVars* globalData, bool first) {
	t-> sick = true;
	t-> remaining_immunity = 0;
	if (globalData->count_turtles_sicks<globalData->count_turtles)
	{
		globalData->count_turtles_sicks++;
	}
	if (!first && globalData->count_turtles_healthy>0)
	{
		globalData->count_turtles_healthy--;
	}
	infectados[t->x][t->y]++;

}

void get_healthy(turtle* t, int** infectados, globalVars* globalData, bool first){
	t-> sick = false;
	t-> remaining_immunity = 0;
	t-> sick_time = 0;
	if (globalData->count_turtles_healthy < globalData->count_turtles)
	{
		globalData->count_turtles_healthy++;
	}
	
	
	if (!first && globalData->count_turtles_sicks > 0)
	{
		globalData->count_turtles_sicks--;
		infectados[t->x][t->y]--;
	}
	
}

void become_immune(turtle* t, globalVars* globalData, int** infectados){
	t->sick = false;
	t->sick_time = 0;
	t->remaining_immunity = globalData->immunity_duration;
	t->immune = true;
	if (globalData->count_turtles_healthy < globalData->count_turtles)
	{
		globalData->count_turtles_inmune++;
		globalData->count_turtles_healthy++;
	}
	
	if (globalData->count_turtles_sicks > 0)
	{
		globalData->count_turtles_sicks--;
	}
	
	if (infectados[t->x][t->y] > 0)
	{
		infectados[t->x][t->y]--;
	}
}

void setup_constants(globalVars* globalData){//Si no se manejan como parámetros
	globalData->infected_percent; // % de la poblacion infectada
    globalData->immune_percent; // % de la poblacion inmune
    globalData->lifespan = 50*52; // vida de una tortuga
    globalData->chance_reproduce = 1; //probabilidad de que una tortuga se reproduzca-> Entero de 0 a 100
    globalData->carrying_capacity = 300; //el número de tortugas que pueden estar en el mundo a la vez
    globalData->immunity_duration = 35; //cuantas semanas dura la inmunidad
    globalData->count_turtles = 0;
    globalData->count_turtles_sicks = 0;
    globalData->count_turtles_inmune = 0;
    globalData->count_turtles_healthy = 0;
    //globalData->duration = 20;				//Duracion del virus. De 0 a 99 semanas
    //globalData->infectiousness = 65;		//Infectividad (Probabilidad) Entero del 1 a 100
    //globalData->number_people = 150;			//Numero de bichos que se crean al inicio. Numero de 10 a 300
    //globalData->chance_recover = 75;
}

void go (casilla** mundo, int** infectados, FILE* archivo, globalVars* globalData){
	time_t tiempo;
	srand((unsigned) time(&tiempo));

	for (int j = 0; j < simTime; j++)
	{
		//Recorre todas las casillas
		for (int i = 0; i < globalData->carrying_capacity; i++)
		{	//Revisa que la posición está llena
			if (!mundo[i]->vacio)
			{
				get_older(mundo[i], infectados, globalData); //Primero aumenta la edad de la tortuga
			}
				
		}
		//Recorre todas las casillas
		for (int i = 0; i < globalData->carrying_capacity; i++)
		{	//Revisa que la posición está llena
			if (!mundo[i]->vacio)
			{
				move(mundo[i]->tortuga, infectados); //Segundo la tortuga cambia de posicion
			}
			
		}
		//Recorre todas las casillas
		for (int i = 0; i < globalData->carrying_capacity; i++)
		{	//Revisa que la posición está llena
			if (!mundo[i]->vacio)
			{
				//Revisa si la tortuga estaba enferma
				if (mundo[i]->tortuga->sick){			
					recover_or_die(mundo[i], infectados, globalData); //Se recupera o muere
				}else{
					reproduce(mundo, globalData, infectados); //Se reproduce
					if (mundo[i]->tortuga->immune == false)
					{
						infect(mundo[i]->tortuga, infectados, globalData); //Infecta
					}
				}
			}	
		}

		update_global_variables(globalData);
		crear_archivo(j, archivo, globalData);
	}
}

void update_global_variables(globalVars* globalData){
	if (globalData->count_turtles > 0){
		globalData->infected_percent = ((float)(globalData->count_turtles_sicks) / (float)(globalData->count_turtles)) * 100;
		globalData->immune_percent = ((float)(globalData->count_turtles_inmune) / (float)(globalData->count_turtles)) * 100;
	}
}

void get_older (casilla* mundo, int** infectados, globalVars* globalData){
	//Turtles die of old age once their age exceeds the lifespan (set at 50 years in this model).

	mundo->tortuga->age = mundo->tortuga->age + 1;//Aumenta la edad
	
	//Verifica si tiene tiempo de vida
	if (mundo->tortuga->age > globalData->lifespan){
		
		//Si estaba enferma, actualiza la matriz de infectados
		if (mundo->tortuga->sick)
		{
			int x = mundo->tortuga->x;
			int y = mundo->tortuga->y;
			
			infectados[x][y]--;
			globalData->count_turtles_sicks--; //Reduce la cantidad de tortugas enfermas

		}else //Si no estaba enferma
		{
			globalData->count_turtles_healthy--; //Reduce la cantidad de tortugas saludables
			if (mundo->tortuga->immune == true)
			{
				globalData->count_turtles_inmune--;
			}
		}
		
		globalData->count_turtles--; //Reduce la cantidad total
		mundo->vacio = true; //Muere
	}

	//Verifica si tiene tiempo de inmunidad y además que no esta enferma 
	if (mundo->tortuga->remaining_immunity > 0){
		mundo->tortuga->remaining_immunity = mundo->tortuga->remaining_immunity - 1; //Reduce el tiempo de inmunidad
	}

	//Verifica si el tiempo de inmunidad se agotó
	if(mundo->tortuga->remaining_immunity == 0 && mundo->tortuga->immune){
		globalData->count_turtles_inmune--; //Reduce la cantidad de tortugas inmunes
		mundo->tortuga->immune = false;		//Dejo de ser inmune
	}

	//Verfica si la tortuga está enferma
	if (mundo->tortuga->sick){ 
		mundo->tortuga->sick_time = mundo->tortuga->sick_time + 1; //Aumenta su tiempo enferma
	}
}

void move (turtle* t, int** infectados){
	// nuevas coordenadas donde se movera la tortuga
	int newX,newY;

	// nuevas coordenadas en X
  	if (t->x > 0 && t->x < 34) newX = rand()%2 - 1; 
	else if (t->x == 0) newX = rand()%1; // si la tortuga esta en el borde izq de la matriz, solo se puede mover a la der o no moverse
	else if (t->x == 34) newX = rand()%1 - 1; // si la tortuga esta en el borde der de la matriz, solo se puede mover a la izq o no moverse

	if (t->y > 0 && t->y < 34) newY = rand()%2 - 1;
	else if (t->y == 0) newY = rand()%1; // si la tortuga esta en el borde inf de la matriz, solo se puede mover hacia arriba o no moverse
	else if (t->y == 34) newY = rand()%1 - 1; // si la tortuga esta en el borde sup de la matriz, solo se puede mover hacia abajo o no moverse

	// aqui se modifica la matriz de infectados dependiendo del movimiento resultante y de si estaba infectada la tort.
	if (t->sick == true && (newX != 0 || newY != 0))  
	{
		infectados[t->x][t->y]--; // la coordenada de origen tiene un infectado menos
		infectados[(t->x)+newX][(t->y)+newY]++; // la coordenada destino tiene un infectado mas
	}
	// asignacion de las nuevas coordenadas
	t->x += newX;
	t->y += newY;
	
}

void infect(turtle* t, int** infectados, globalVars* globalData){ 
	for(int i=0;i<infectados[t->x][t->y];i++){
		if (rand()%100 < globalData->infectiousness && t->sick==0 && t->remaining_immunity==0)
			get_sick(t, infectados, globalData, false);
		if (t->sick==1||t->remaining_immunity > 0)
			i=infectados[t->x][t->y];		//Brake
	}
}

 //~ Once the turtle has been sick long enough, it either recovers (and becomes immune) or it dies.
void recover_or_die (casilla* mundo, int** infectados, globalVars* globalData){
	if (mundo->tortuga->sick_time > globalData->duration){      //If the turtle has survived past the virus' duration, then either recover or die
		if (rand()%100 < globalData->chance_recover){	//Parametro chance recover ingresado desde afuera de 0 a 100
			become_immune(mundo->tortuga, globalData, infectados);
		}else{
			//~ die(t); 
			mundo->vacio=true;
			infectados[mundo->tortuga->x][mundo->tortuga->y]--;
			globalData->count_turtles--;
			globalData->count_turtles_sicks--;
		}
	}
}

void reproduce (casilla** mundo, globalVars* globalData, int** infectados){
	// una tortuga se rerproduce
	int x,y;
	if (globalData->count_turtles < globalData->carrying_capacity && rand()%100 < globalData->chance_reproduce){
		turtle* babyturtle = new_turtle(infectados, globalData, true);
		babyturtle->age = 0;		
		
		// este for es para encontrarle un lugar vacio a la nueva tortuga en el arreglo de casillas
		for (int i = 0; i < globalData->carrying_capacity; i++)
		{
			if (mundo[i]->vacio)
			{
				mundo[i]->tortuga = babyturtle;
				mundo[i]->vacio = false;
				i = globalData->carrying_capacity;
			}
			
		}
		
	}
	
}

void crear_archivo(int semana, FILE* archivo, globalVars* globalData){

	
	fprintf(archivo, "%d\t%d\t%d\t%d\t%d\n", semana, globalData->count_turtles_sicks, globalData->count_turtles_inmune, globalData->count_turtles_healthy, globalData->count_turtles);

	if (semana == simTime)
	{
		fclose(archivo);
	}
}

void reset_ticks(){}
