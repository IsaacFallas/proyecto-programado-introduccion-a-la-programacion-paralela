#include <stdio.h>
#include <stdlib.h>
#include "../include/virus.h"

int main(int argc, char *argv[]){

	int i;
	int j;
	int** infectados;
	infectados = (int**)malloc(sizeof(int*)*35);
	for (i=0; i<35;i++)
		infectados[i] = (int*)malloc(sizeof(int)*35);
	for (i=0;i<35;i++){
		for (j=0;j<35;j++){
			infectados[i][j]=0;
		}
	}
	
	casilla** mundo; 
	mundo = (casilla**)malloc(300*sizeof(casilla*));

	for (int i = 0; i < 300; i++)
	{
		mundo[i] = (casilla*)malloc(sizeof(casilla));
		mundo[i]->tortuga = (turtle*)malloc(sizeof(turtle));

	}

	globalVars globalData;

	for (int i = 0; i < argc; i++){
		if (argv[i][0] == '-' && argv[i][1] == 'd'){
		globalData.duration = atoi(argv[i+1]);
		}
		if (argv[i][0] == '-' && argv[i][1] == 'i'){
		globalData.infectiousness = atoi(argv[i+1]);
		}
		if (argv[i][0] == '-' && argv[i][1] == 'n'){
		globalData.number_people = atoi(argv[i+1]);
		}
		if (argv[i][0] == '-' && argv[i][1] == 'r'){
		globalData.chance_recover = atoi(argv[i+1]);
		}
  	}

	/*globalData.duration = 20;				//Duracion del virus. De 0 a 99 semanas
    globalData.infectiousness = 10;		//Infectividad (Probabilidad) Entero del 1 a 100
    globalData.number_people = 150;			//Numero de bichos que se crean al inicio. Numero de 10 a 300
    globalData.chance_recover = 50;*/
		
	setup_constants(&globalData);

	setup_turtles(mundo, infectados, &globalData);		//No se si llevan &
	
	FILE* archivo = fopen("../P3/datos.txt", "w");

	go(mundo, infectados, archivo, &globalData);
	
	printf("Done\n");

//return 0;

}
