import matplotlib.pyplot as plt 

with open('datos.txt') as f:
    lines = f.readlines()
    semanas = [line.split()[0] for line in lines]
    sick = [line.split()[1] for line in lines]
    immune = [line.split()[2] for line in lines]
    healthy = [line.split()[3] for line in lines]
    tortugas = [line.split()[4] for line in lines]


plt.plot(semanas, sick, label = "Tortugas enfermas") 

plt.plot(semanas, immune, label = "Tortugas inmunes")

plt.plot(semanas, healthy, label = "Tortugas saludables") 

plt.plot(semanas, tortugas, label = "Tortugas totales") 

# naming the x axis 
plt.xlabel('Semanas') 
# naming the y axis 
plt.ylabel('Cantidad de tortugas') 
# giving a title to my graph 
plt.title('Modelo de un virus') 

# show a legend on the plot 
plt.legend() 

# function to show the plot 
plt.show() 