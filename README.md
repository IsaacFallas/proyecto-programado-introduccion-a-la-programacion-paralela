# Proyecto programado Introduccion a la programacion paralela

## Estudiantes
* Jorge Isaac Fallas Mejía B62562
* José Fabián Marcenaro Larios B64069
* Josue Hernandez Calderon B63326

# Ejecución del programa

* Para ejecutar el programa sin pthreads vaya a la carpeta P1 y ejecute el make.
* Para usar pthreads ejecute el make de P2.
* Para gráficar los datos con python ejecute "python *.py" en P3.
